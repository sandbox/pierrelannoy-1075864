# $Id$

Abstract
========

The ning_integration module is currently a single module whose purpose is to
allow authentication via one or more Ning networks. This module allows:
- mixed authentication (Drupal + Ning) or exclusive (only Ning);
- authentication via multiple networks (with precedence management);
- synchronization of passwords (optional);
- adaptation of Drupal standard forms;
- roles mapping between the two systems.

Installation
============
1. Unpack the ning_integration folder and contents in the appropriate modules
   directory of your Drupal installation.  This is probably
   sites/all/modules/
2. Download the two following files : NingApi.php and OAuth.php from 
   https://github.com/ning/ning-api-php/tree/3f86d00ee791d4c39165003090b66d24fe5b3a4f/src
   and copy them to the ning_integration/includes directory.
2. Enable the ning_integration module in the administration tools.
3. If you're not using Drupal's default administrative account, make
   sure "administer ning integration" is enabled through access control
   administration.
4. Visit the Ning settings page and make appropriate configurations.

Misc.
=====
This module is sponsored by Voile-RC (www.voile-rc.com), the french social
network for RC sailing enthusiasts.