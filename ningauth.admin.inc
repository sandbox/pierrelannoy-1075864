<?php
// $Id$

/**
 * @file
 * Module admin page callbacks.
 */

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function ningauth_admin_settings() {
  $options_login_process = array(
    NINGAUTH_AUTH_MIXED => t('Mixed mode. The Ning authentication is performed only if Drupal authentication fails.'),
    NINGAUTH_AUTH_EXCLUSIVED => t('Exclusive mode. Only the Ning authentication is performed.'),
  );
  $options_login_conflict = array(
    NINGAUTH_CONFLICT_DISALLOW => t('Disallow login'),
    NINGAUTH_CONFLICT_ALLOW_NING => t('Do login with Ning credentials'),
    NINGAUTH_CONFLICT_ALLOW_DRUPAL => t('Do login with Drupal credentials'),
  );
  $form['system-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication Mode'),
    '#description' => t('<strong>NOTE:</strong> These settings have no effect on Drupal <i>admin</i> account (uid=1) :  this account never uses Ning authentication.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['system-options']['ningauth_login_process'] = array(
    '#type' => 'radios',
    '#title' => t('Choose authentication mode'),
    '#default_value' => NINGAUTH_LOGIN_PROCESS,
    '#options' => $options_login_process,
    '#required' => TRUE,
  );
  $form['system-options']['ningauth_login_conflict'] = array(
    '#type' => 'radios',
    '#title' => t('Choose user conflict resolve procedure'),
    '#description' => t('Set what should be done if both the local Drupal account and Ning account have the same login name.'),
    '#default_value' => NINGAUTH_LOGIN_CONFLICT,
    '#options' => $options_login_conflict,
    '#required' => TRUE,
  );
  $form['security-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Security Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['security-options']['ningauth_sync_passwords'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync Drupal password with the Ning password'),
    '#default_value' => NINGAUTH_SYNC_PASSWORDS,
    '#description' => t('If checked, the Drupal password will be syncronized with the Ning password. This might be useful if some other modules need to authenticate against the user password hash stored in Drupal. It might introduce security issues since after deletion of the Ning account the user still be able to login to Drupal with his password. If unsure, leave this unchecked.'),
  );
  $form['security-options']['ningauth_public_profile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Public profiles'),
    '#default_value' => NINGAUTH_PUBLIC_PROFILE,
    '#description' => t('Make non sensitive informations about network membership viewable by everybody.'),
  );
  $form['ning-ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Interface Options'),
    '#description' => t('<p>Alters Ning authenticated user interface only, though admin accounts can still access email and password fields of Ning users regardless of selections. Does not effect non-Ning authenticated accounts. </p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['ning-ui']['ningauth_disable_pass_change'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove password change fields'),
    '#default_value' => NINGAUTH_DISABLE_PASS_CHANGE,
    '#description' => t('<p>Remove password change fields from user edit form. Request new password feature will be disabled for all users even for the Drupal admin account.</p>'),
  );
  $form['ning-ui']['ningauth_disable_picture_change'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove picture change fields'),
    '#default_value' => NINGAUTH_DISABLE_PICTURE_CHANGE,
    '#description' => t('<p>Remove picture change fields from user edit form.</p>'),
  );
  $options_email_field = array(
    NINGAUTH_EMAIL_FIELD_NO => t('Do nothing'),
    NINGAUTH_EMAIL_FIELD_REMOVE => t('Remove email field from form'),
    NINGAUTH_EMAIL_FIELD_DISABLE => t('Disable email field on form'),
  );
  $form['ning-ui']['ningauth_alter_email_field'] = array(
    '#type' => 'radios',
    '#title' => t('Alter email field'),
    '#description' => t('Set visibility of email field (user edit form) for Ning authenticated users.'),
    '#default_value' => NINGAUTH_ALTER_EMAIL_FIELD,
    '#options' => $options_email_field,
    '#required' => TRUE,
  );
  $form['ning-ui']['ningauth_show_realname'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show member name'),
    '#default_value' => NINGAUTH_SHOW_REALNAME,
    '#description' => t('<p>When possible, shows member name instead of Drupal user name.</p>'),
  );
  $form['ning-ui']['ningauth_show_network'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show network name'),
    '#default_value' => NINGAUTH_SHOW_NETWORK,
    '#description' => t('<p>Add the network name after the user name.</p>'),
  );
  $form['ning-ui']['ningauth_ning_avatar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show network avatar'),
    '#default_value' => NINGAUTH_NING_AVATAR,
    '#description' => t('<p>If enabled in <a href="?q=admin/build/themes/settings">themes settings</a>, shows network avatar instead of Drupal user picture.</p>'),
  );
  $form['ning-ui']['ningauth_ning_profile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect to member page'),
    '#default_value' => NINGAUTH_NING_PROFILE,
    '#description' => t('<p>When clicking on usernames, redirects to network member page instead of Drupal user page.</p>'),
  );
  $form['ning-debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Troubleshooting Options'),
    '#description' => t('<p>Option to helps in case of issues with Ning APIs.</p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['ning-debug']['ningauth_log_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active verbose mode'),
    '#default_value' => NINGAUTH_LOG_DEBUG,
    '#description' => t('<p>Log every access to Ning APIs.</p>'),
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset to defaults'),
  );
  return $form;
}

/**
 * Submit hook for the settings form.
 */
function ningauth_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      variable_set('ningauth_login_process', $values['ningauth_login_process']);
      variable_set('ningauth_login_conflict', $values['ningauth_login_conflict']);
      variable_set('ningauth_sync_passwords', $values['ningauth_sync_passwords']);
      variable_set('ningauth_disable_pass_change', $values['ningauth_disable_pass_change']);
      variable_set('ningauth_disable_picture_change', $values['ningauth_disable_picture_change']);
      variable_set('ningauth_alter_email_field', $values['ningauth_alter_email_field']);
      variable_set('ningauth_log_debug', $values['ningauth_log_debug']);
      variable_set('ningauth_show_realname', $values['ningauth_show_realname']);
      variable_set('ningauth_show_network', $values['ningauth_show_network']);
      variable_set('ningauth_ning_avatar', $values['ningauth_ning_avatar']);
      variable_set('ningauth_ning_profile', $values['ningauth_ning_profile']);
      variable_set('ningauth_public_profile', $values['ningauth_public_profile']);


      drupal_set_message(t('The configuration options have been saved.'));
      break;
    case t('Reset to defaults'):
      variable_del('ningauth_login_process');
      variable_del('ningauth_login_conflict');
      variable_del('ningauth_sync_passwords');
      variable_del('ningauth_disable_pass_change');
      variable_del('ningauth_disable_picture_change');
      variable_del('ningauth_alter_email_field');
      variable_del('ningauth_log_debug');
      variable_del('ningauth_show_realname');
      variable_del('ningauth_show_network');
      variable_del('ningauth_ning_avatar');
      variable_del('ningauth_ning_profile');
      variable_del('ningauth_public_profile');
      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
  }
  menu_rebuild();
}

/**
 * Implements the networks list.
 *
 * @return
 *   The HTML table with the servers list.
 */
function ningauth_admin_list() {
  $form['list'] = array();
  $result = db_query("SELECT sid, name, status, weight FROM {ningauth} ORDER BY weight");
  while ($row = db_fetch_object($result)) {
    $form['list'][$row->sid] = array(
      'name' => array('#value' => $row->name),
      'status' => array('#value' => $row->status),
      'weight' => array(
        '#type' => 'weight',
        '#name' => 'sid_'. $row->sid .'_weight',
        '#delta' => 10,
        '#default_value' => $row->weight,
      ),
    );
  }
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit hook for the networks list form.
 */
function ningauth_admin_list_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  switch ($op) {
    case t('Save'):
      foreach ($form_state['clicked_button']['#post'] as $name => $val) {
        if (preg_match('/^sid_.*_weight/', $name)) {
          $sid = preg_replace(array('/^sid_/', '/_weight$/'), array('', ''), $name);
          db_query("UPDATE {ningauth} SET weight = %d WHERE sid = %d", $val, $sid);
        }
      }
      break;
  }
}

/**
 * Implements the network edit page.
 *
 * @param $form_state
 *   A form state array.
 * @param $op
 *   An operatin - add or edit.
 * @param $sid
 *   A network ID.
 *
 * @return
 *   The form structure.
 */
function ningauth_admin_form(&$form_state, $op = NULL, $sid = NULL) {
  if ($op == "edit" && $sid) {
    $edit = db_fetch_array(db_query("SELECT * FROM {ningauth} WHERE sid = %d", $sid));
    $form['sid'] = array(
      '#type' => 'hidden',
      '#value' => $sid,
    );
  }
  else {
    $edit = array(
      'name' => '',
      'domain' => '',
      'subdomain' => '',
      'ckey' => '',
      'csecret' => ''
    );
  }
  $form['server-settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Network settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['server-settings']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $edit['name'],
    '#description' => t('Choose a <em><strong>unique</strong></em> name for this network configuration.'),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['server-settings']['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => $edit['domain'],
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('The full network URL (eg. http://www.example.com).'),
    '#required' => TRUE,
  );
  $form['login-procedure'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ning API'),
    '#description' => t('For these items, you must go to the backend of your network. Then choose "Ning APIs" and click on "Add Key".'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['login-procedure']['subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#default_value' => $edit['subdomain'],
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('The subdomain for this network. If the Ning URL of this network is http://example.ning.com, subdomain is <i>example</i>.'),
    '#required' => TRUE
  );
  $form['login-procedure']['ckey'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer key'),
    '#default_value' => $edit['ckey'],
    '#size' => 36,
    '#maxlength' => 36,
    '#description' => t('The consumer key as indicated in the network backend.'),
    '#required' => TRUE
  );
  $form['login-procedure']['csecret'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer secret'),
    '#default_value' => $edit['csecret'],
    '#size' => 36,
    '#maxlength' => 36,
    '#description' => t('The consumer secret as indicated in the network backend.'),
    '#required' => TRUE
  );
  $default_owner_role_options = array();
  if ($sid) {
    $result = db_query("SELECT rid FROM {ningauth_roles} WHERE sid = %d AND userstate = 0", $sid);
    while ($role = db_fetch_object($result)) {
      $default_owner_role_options[] = $role->rid;
    }
  }
  $default_moderator_role_options = array();
  if ($sid) {
    $result = db_query("SELECT rid FROM {ningauth_roles} WHERE sid = %d AND userstate = 1", $sid);
    while ($role = db_fetch_object($result)) {
      $default_moderator_role_options[] = $role->rid;
    }
  }
  $default_member_role_options = array();
  if ($sid) {
    $result = db_query("SELECT rid FROM {ningauth_roles} WHERE sid = %d AND userstate = 2", $sid);
    while ($role = db_fetch_object($result)) {
      $default_member_role_options[] = $role->rid;
    }
  }
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  $role_options = array();
  while ($role = db_fetch_object($result)) {
    if ($role->rid>2) {
      $role_options[$role->rid] = $role->name;
    }
  }
  $form['role-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t('<p>The Drupal <i>authenticated user</i> role (rid=2) is always assigned to users unless the following:<ul><li>user was banned from the network;</li><li>user is waiting for approval;</li><li>user have left the network.</li></lu></p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['role-options']['owner-role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Give the network owner these additional roles'),
    '#default_value' => $default_owner_role_options,
    '#options' => $role_options,
  );
  $form['role-options']['moderator-role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Give the network moderators these additional roles'),
    '#default_value' => $default_moderator_role_options,
    '#options' => $role_options,
  );
  $form['role-options']['member-role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Give the network members these additional roles'),
    '#default_value' => $default_member_role_options,
    '#options' => $role_options,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validate hook for the network form.
 */
function ningauth_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!isset($values['sid'])) {
    if (db_fetch_object(db_query("SELECT name FROM {ningauth} WHERE name = '%s'", $values['name']))) {
      form_set_error('name', t('A network with the name %name already exists.', array('%name' => $values['name'])));
    }
  }
}

/**
 * Submit hook for the network form.
 */
function ningauth_admin_form_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      if (!isset($values['sid'])) {
        db_query("INSERT INTO {ningauth} (name, status, domain, subdomain, ckey, csecret) VALUES ('%s', %d, '%s', '%s', '%s', '%s')", $values['name'], 1, $values['domain'], $values['subdomain'], $values['ckey'], $values['csecret']);
        $result = db_query("SELECT sid FROM {ningauth} WHERE name = '%s'", $values['name']);
        if ($row = db_fetch_object($result)) {
            $sid = $row->sid;
        }
      }
      else {
        db_query("UPDATE {ningauth} SET name = '%s', domain = '%s', subdomain = '%s', ckey = '%s', csecret = '%s' WHERE sid = %d", $values['name'], $values['domain'], $values['subdomain'], $values['ckey'], $values['csecret'], $values['sid']);
        $sid = $values['sid'];
      }
      db_query("DELETE FROM {ningauth_roles} WHERE sid = %d", $sid);
      foreach (array_filter($form_state['values']['owner-role']) as $rid) {
        db_query("INSERT INTO {ningauth_roles} (rid, userstate, sid) VALUES (%d, 0, %d)", $rid, $sid);
      }
      foreach (array_filter($form_state['values']['moderator-role']) as $rid) {
        db_query("INSERT INTO {ningauth_roles} (rid, userstate, sid) VALUES (%d, 1, %d)", $rid, $sid);
      }
      foreach (array_filter($form_state['values']['member-role']) as $rid) {
        db_query("INSERT INTO {ningauth_roles} (rid, userstate, sid) VALUES (%d, 2, %d)", $rid, $sid);
      }
      drupal_set_message(t('Network %name has been added.', array('%name' => $values['name'])));
      watchdog('ningauth', 'Network %name has been added.', array('%name' => $values['name']));
      $form_state['redirect'] = 'admin/settings/ning/ningauth/list';
      break;
  }
}

/**
 * De-activates the network.
 *
 * @param $form_State
 *   A form_state array.
 * @param $sid
 *   A network ID.
 *
 * @return
 *  Form array.
 */
function ningauth_admin_deactivate(&$form_state, $sid) {
  if (is_numeric($sid) && ($name = db_result(db_query("SELECT name from {ningauth} WHERE sid = %d", $sid)))) {
    $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
    return confirm_form($form, t('Are you sure you want to de-activate the network %name?', array('%name' => $name)), 'admin/settings/ning/ningauth/list', '', t('De-activate'), t('Cancel'));
  }
  else {
    drupal_not_found();
    exit;
  }
}

/**
 * De-activates the network.
 *
 * @return
 */
function ningauth_admin_deactivate_submit($form, &$form_state) {
  $sid = $form_state['values']['sid'];
  $result = db_query("SELECT name from {ningauth} WHERE sid = %d", $sid);
  if ($row = db_fetch_object($result)) {
    db_query("UPDATE {ningauth} SET status = '0' WHERE sid = %d", $sid);
    drupal_set_message(t('Network %name has been de-activated.', array('%name' => $row->name)));
    watchdog('ningauth', 'Network %name has been de-activated.', array('%name' => $row->name));
  }
  drupal_goto('admin/settings/ning/ningauth/list');
}

/**
 * Activates the network.
 *
 * @param $form_State
 *   A form_state array.
 * @param $sid
 *   A network ID.
 *
 * @return
 *  Form array.
 */
function ningauth_admin_activate(&$form_state, $sid) {
  if (is_numeric($sid) && ($name = db_result(db_query("SELECT name from {ningauth} WHERE sid = %d", $sid)))) {
    $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
    return confirm_form($form, t('Are you sure you want to activate the network %name?', array('%name' => $name)), 'admin/settings/ning/ningauth/list', '', t('Activate'), t('Cancel'));
  }
  else {
    drupal_not_found();
    exit;
  }
}

/**
 * Activates the network.
 *
 * @return
 */
function ningauth_admin_activate_submit($form, &$form_state) {
  $sid = $form_state['values']['sid'];
  $result = db_query("SELECT name from {ningauth} WHERE sid = %d", $sid);
  if ($row = db_fetch_object($result)) {
    db_query("UPDATE {ningauth} SET status = '1' WHERE sid = %d", $sid);
    drupal_set_message(t('Network %name has been activated.', array('%name' => $row->name)));
    watchdog('ningauth', 'Network %name has been activated.', array('%name' => $row->name));
  }
  drupal_goto('admin/settings/ning/ningauth/list');
}

/**
 * Implements the network delete page.
 *
 * @param $form_state
 *   A form state array.
 * @param $sid
 *   A network ID.
 *
 * @return
 *   The form structure.
 */
function ningauth_admin_delete(&$form_state, $sid) {
  if (is_numeric($sid) && ($name = db_result(db_query("SELECT name from {ningauth} WHERE sid = %d", $sid)))) {
    $form = array(
      'sid' => array('#type' => 'hidden', '#value' => $sid),
      'name' => array('#type' => 'hidden', '#value' => $name),
    );

    return confirm_form($form, t('Are you sure you want to delete the network %name?', array('%name' => $name)), 'admin/settings/ning/ningauth/list', NULL, t('Delete'), t('Cancel'));
  }
  else {
    drupal_not_found();
    exit;
  }
}

/**
 * Submit hook for the network delete page.
 */
function ningauth_admin_delete_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['confirm'] && $values['sid']) {
    db_query("DELETE FROM {ningauth} WHERE sid = %d", $values['sid']);
    db_query("DELETE FROM {ningauth_roles} WHERE sid = %d", $values['sid']);
    drupal_set_message(t('Network %name has been deleted.', array('%name' => $values['name'])));
    watchdog('ningauth', 'Network %name has been deleted.', array('%name' => $values['name']));
  }
  drupal_goto('admin/settings/ning/ningauth/list');
}

/**
 * Implements the Ning admin page.
 *
 * @return
 *   The themed HTML page.
 */
function ningauth_admin_menu_block_page() {
  return theme('admin_block_content', system_admin_menu_block(menu_get_item()));
}